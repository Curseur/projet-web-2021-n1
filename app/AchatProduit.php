<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchatProduit extends Model
{
    protected $fillable = ['name', 'total_price'];
}
