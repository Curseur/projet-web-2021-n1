<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['payment_id'];

    public function achat() {
        return $this->belongsTo(Achat::class);
    }
}
