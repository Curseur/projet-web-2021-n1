<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $fillable = ['name', 'description', 'plateform', 'price', 'code'];

    public function notices() {
        return $this->hasMany(Notice::class);
    }

    public function achat() {
        return $this->belongsTo(Achat::class);
    }
}