<?php

namespace App\Http\Controllers;

use App\Notice;
use App\Produit;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $notice = Notice::all();
        $search = $request->get('search');
        if ($search) {
            $notice = Notice::where('avis', 'like', '%' . $search . '%')
            ->orWhere('note', 'like', '%' . $search . '%')
            ->get();
        } else {
            $notice = Notice::all();
        }

        return view('notice.index', ['notice' => $notice, 'search' => $search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Produit $produit)
    {
        $produit_id = $request->input('produit');

        $id = $request->input('id');
        return view('notice.createnotice', ['id' => $id, 'produit' => Produit::find($produit_id)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notice = new Notice;
        $notice->avis = $request->input('avis');
        $notice->note = $request->input('note');
        $notice->produit_id = $request->input('produit_id');
        $notice->save();

        $produit = Produit::find($request->input('produit_id'));
        return redirect()->route('produit.show', ['produit' => $produit]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function show(Notice $notice)
    {
        return view('notice.show', ['notice' => $notice]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit(Notice $notice)
    {
        $produit = Produit::all();
        return view('notice.edit', ['notice' => $notice, 'produit' => $produit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notice $notice)
    {
        $avis = $request->input('avis');
        $note = $request->input('note');
        $produit_id = $request->input('produit');

        $notice = Notice::find($notice->id);
        $notice->avis = $avis;
        $notice->note = $note;
        $notice->produit_id = $produit_id;
        $notice->push();
        return redirect()->route('notice.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notice $notice)
    {
        $notice->delete();
        return redirect()->route('notice.index');
    }
}
