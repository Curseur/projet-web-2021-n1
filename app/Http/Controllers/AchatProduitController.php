<?php

namespace App\Http\Controllers;

use App\AchatProduit;
use Illuminate\Http\Request;

class AchatProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AchatProduit  $achatProduit
     * @return \Illuminate\Http\Response
     */
    public function show(AchatProduit $achatProduit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AchatProduit  $achatProduit
     * @return \Illuminate\Http\Response
     */
    public function edit(AchatProduit $achatProduit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AchatProduit  $achatProduit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AchatProduit $achatProduit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AchatProduit  $achatProduit
     * @return \Illuminate\Http\Response
     */
    public function destroy(AchatProduit $achatProduit)
    {
        //
    }
}
