<?php

namespace App\Http\Controllers;

use App\Produit;
use App\Notice;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $produit = Produit::all();
        $search = $request->get('search');
        if ($search) {
            $produit = Produit::where('name', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%')
            ->orWhere('plateform', 'like', '%' . $search . '%')
            ->orWhere('price', 'like', '%' . $search . '%')
            ->get();
        } else {
          $produit = Produit::all();
        }

        return view('produit.index', ['produits' => $produit, 'search' => $search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produit = new Produit;
        $produit->name = $request->input('name');
        $produit->description = $request->input('description');
        $produit->plateform = $request->input('plateform');
        $produit->price = $request->input('price');
        $produit->code = $request->input('code');
        $produit->save();

        return redirect()->action([ProduitController::class, 'index']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {
        return view('produit.show', ['prod' => $produit]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        return view('produit.edit', ['prod' => $produit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit $produit)
    {
        $prod = Produit::find($produit->id);
        $prod->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'plateform' => $request->input('plateform'),
            'price' => $request->input('price'),
            'code' => $request->input('code'),
        ]);

        return redirect()->action([ProduitController::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produit $produit)
    {
        $produit->notices()->delete();
        $produit->delete();
        return redirect()->action([ProduitController::class, 'index']);
    }
}
