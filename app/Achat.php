<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achat extends Model
{
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function produits() {
        return $this->hasMany(Produit::class);
    }

    public function payment_infos() {
        return $this->hasOne(Payment::class);
    }

    public function getPaymentText($value) {
        $texts = ["carte" => "Carte bancaire"];

        return $texts[$this->payment];
    }

    public function getTotalAchat() {
        return $this->total;
    }
}
