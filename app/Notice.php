<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $fillable = ['avis', 'note'];

    public function produit() {
        return $this->belongsTo(Produit::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
