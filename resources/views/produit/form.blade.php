@extends('application')
@section('page-title')
  @yield('page-title')
@endsection
@section('page-content')
  <div class="container mb-5 mt-3">
    <form method="post" action="@yield('action')">
      @yield('method')
      @csrf
        <div class="form-group">
          <label for="name">Game Name</label>
          <input type="text" class="form-control" id="name" name="name" value="@yield('name')">
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <input type="text" class="form-control" name="description" id="description" value="@yield("description")">
        </div>
        <div class="form-group">
          <label for="plateform">Plateform</label>
          <select class="form-control" name="plateform" id="plateform">
            <option>PC (Master Race)</option>
            <option>Xbox Series X</option>
            <option>Xbox ONE</option>
            <option>PS5</option>
            <option>PS4</option>
          </select>
        </div>
        <div class="form-group">
          <label for="price">Price</label>
          <input type="number" class="form-control" name="price" id="price" value="@yield("price")">
        </div>
        <div class="form-group">
          <label for="code">Code</label>
          <input type="text" class="form-control" name="code" id="code" value="@yield("code")">
        </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
@endsection
