@extends('application')
@section('page-title')
  {{ $prod->name }}
@endsection

@section('page-content')
  <br>
  <div class="container">
    <div class="d-flex">
      <a class="btn btn-outline-primary mr-2" href="{{ route("produit.index") }}">Back</a>
    </div>
    <br>
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">{{ $prod->name }} - @foreach($prod->notices as $noti){{ $noti->note }}@endforeach /10</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{ $prod->price }} €</h6>
        <p class="card-text">
            <strong>Description: </strong>
            {{ $prod->description }}
        </p>
        <p class="card-text">
          <strong>Plateform: </strong>
        </p>
        <ul class="list-group list-group-flush mb-3">
          {{ $prod->plateform }}
        </ul>
        <p class="card-text">
          <strong>Notices: </strong>
        </p>
        <ul class="list-group list-group-flush mb-3">
            @foreach($prod->notices as $noti)
                <li class="list-group-item">
                    <a href="{{ route("notice.show", $noti) }}">{{ $noti->avis/*." ".$memb->firstname." ".$memb->lastname*/ }}</a>
                </li>
            @endforeach
        </ul>
        <div class="d-flex">
            <a class="btn btn-outline-primary mr-2" href="{{ route("notice.create", ["produit"=>$prod]) }}">Add Notice</a>
            <a class="btn btn-outline-info mr-2" href="{{ route("produit.edit", $prod) }}">Edit</a>
            <form action="{{ route("produit.destroy", $prod->id) }}" method="post">
              <input class="btn btn-outline-danger" type="submit" value="Delete"/>
              @method('delete')
              @csrf
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
