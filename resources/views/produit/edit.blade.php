@extends('produit.form')
@section('page-title')
    Edit Game - {{ $prod->name }}
@endsection
@section('action'){{ route('produit.update', $prod) }}@endsection
@section('method') @method("PUT") @endsection
@section('name'){{ $prod->name }}@endsection
@section('description'){{ $prod->description }}@endsection
@section('plateform'){{ $prod->plateform }}@endsection
@section('price'){{ $prod->price }}@endsection
@section('code'){{ $prod->code }}@endsection