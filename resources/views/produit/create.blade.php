@extends('produit.form')
@section('page-title')
    Add New Game
@endsection
@section('action')
    {{ route('produit.store') }}
@endsection