@extends('application')
@section('page-title')
    All Games
@endsection
@section('page-content')
    <div class="container">
        @if($search)
            <p class="mt-3">Search result for: {{ $search }}</p>
            <a href="{{route("produit.index")}}" class="mb-5">Return to list</a>
        @endif
    </div>
    <div class="container">

        <a class="btn btn-outline-success mt-3" href="{{ route("produit.create") }}">Add Game</a>

        <table class="table table-bordered mt-3">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Plateform</th>
                <th scope="col">Price</th>
                <th scope="col">Note</th>
                <th scope="col">Go to...</th>
            </tr>
            </thead>
            <tbody>

            @foreach($produits as $prod)
                <tr>
                    <td>{{ $prod->name }}</td>
                    <td>{{ $prod->description }}</td>
                    <td>{{ $prod->plateform }}</td>
                    <td>{{ $prod->price }} €</td>
                    <td>
                        @foreach($prod->notices as $noti)
					        <li class="list-group" aria-current="true">{{ $noti->note }} /10</li>
                        @endforeach
                    </td>
                    <td class="d-flex" style="size: inherit">
                        <a class="btn btn-outline-success mr-2" href="{{ route("produit.show", $prod) }}">Check this game</a>
                        <a class="btn btn-outline-info mr-2" href="{{ route("produit.edit", $prod) }}">Edit</a>
                        <form action="{{ route("produit.destroy", $prod->id) }}" method="post">
                            <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                            @method('delete')
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection
