<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand text-white" href="{{ route("produit.index") }}">EgameCenter</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link text-white" href="{{ route("produit.index") }}">All Games</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="{{ route("notice.index") }}">All Recent Notices</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="">Your Shopping Cart</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="">Space Membre</a>
          </li>
          {{--<li class="nav-item dropdown">
            <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Register | Sign In
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="nav-link text-white" href="">Register</a></li>
              <li><hr class="dropdown-divider"></li>
              <li><a class="nav-link text-white" href="">Sign In</a></li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="">Sign Out</a>
          </li>--}}
          <li class="nav-item dropdown">
            <a class="nav-link text-white dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
              Register | Sign In
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="">Register</a>
              <a class="dropdown-item" href="">Sign In</a>
            </div>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" method="get">
          <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </div>
  </nav>