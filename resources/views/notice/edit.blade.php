@extends("notice.form")
@section("page-title")
    Edit Notice
@endsection
@section("action"){{ route("notice.update", $notice) }}@endsection
@section("method") @method("PUT") @endsection
@section("avis"){{ $notice->avis }}@endsection
@section("note"){{ $notice->note }}@endsection
@section('produit')
    <select name="produit" class="form-control">
        @foreach ($produit as $prod)
            <option value="{{ $prod->id }}">{{ $prod->name." ".$notice->note }}</option>
        @endforeach
    </select>
@endsection