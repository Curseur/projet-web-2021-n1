@extends('application')
@section('page-title')
    @yield('page-title')
@endsection
@section('page-content')
    <div class="container mb-5 mt-3">
        <form method="post" action="@yield('action')">
            @csrf
            <div class="form-row">
                <div class="col">
                    <label for="avis">Notice</label>
                    <input type="text" class="form-control" name="avis" id="avis" value="@yield("avis")">
                </div>
                <div class="form-group">
                    <label for="note">Note</label>
                    <select class="form-control" name="note" id="note">
                      <option>0</option>
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      <option>6</option>
                      <option>7</option>
                      <option>8</option>
                      <option>9</option>
                      <option>10</option>
                    </select>
                </div>
            </div>
            <label for="produit">Product</label>
            <fieldset>
                <div class="form-row mb-5">
                  <div class="col">
                    @yield("produit")
                  </div>
                </div>
            </fieldset>
            <input type="hidden" name="produit_id" value="@yield("produit_id")">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection