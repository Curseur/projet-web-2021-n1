@extends('application')
@section('page-title')
    All Notices
@endsection
@section('page-content')
    <div class="container">
        @if($search)
            <p class="mt-3">Search result for: {{ $search }}</p>
            <a href="{{route("notice.index")}}" class="mb-5">Return to list</a>
        @endif
    </div>
    <div class="container">

        <table class="table table-bordered mt-3">
            <thead>
            <tr>
                <th scope="col">Avis</th>
                <th scope="col">Note</th>
                <th scope="col">Notice for...</th>
                <th scope="col">Options</th>
            </tr>
            </thead>
            <tbody>

            @foreach($notice as $noti)
                <tr>
                    <td>{{ $noti->avis }}</td>
                    <td>{{ $noti->note }} /10</td>
                    <td>{{ $noti->produit->name}}</td>
                    <td class="d-flex" style="size: inherit">
                        <a class="btn btn-outline-success mr-2" href="{{ route("notice.show", $noti) }}">Show</a>
                        <a class="btn btn-outline-info mr-2" href="{{ route("notice.edit", $noti) }}">Edit</a>
                        <form action="{{ route("notice.destroy", $noti->id) }}" method="post">
                            <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                            @method('delete')
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection
