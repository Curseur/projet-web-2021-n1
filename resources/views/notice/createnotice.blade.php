@extends("notice.formnotice")
@section("page-title")
	Add Notice
@endsection
@section("action")
	{{ route("notice.store") }}
@endsection
@section('produit')
	{{ $produit->name."  ".$produit->plateform }}
@endsection
@section('produit_id')
	{{ $produit->id }}
@endsection