@extends('application')
@section('page-title')
    Notice for {{ $notice->produit->name }}
@endsection

@section('page-content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{--{{ $notice->member->lastname }} {{ $notice->member->firstname }}--}}</h5>
                <p class="card-text">
                    <strong>Note: </strong>
                </p>
                <h6 class="card-subtitle mb-2 text-muted">{{ $notice->note }} /10</h6>
                <p class="card-text">
                    <strong>Notice: </strong>
                    {{ $notice->avis }}
                </p>
                <div class="d-flex">
                    <a class="btn btn-outline-info mr-2" href="{{ route("notice.edit", $notice) }}">Edit Notice</a>
                    <form action="{{ route("notice.destroy", $notice->id) }}" method="post">
                        <input class="btn btn-outline-danger" type="submit" value="Delete"/>
                        @method('delete')
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection